class ModelRagister {
  String email;
  String pass;
  String gender;
  String mobile;
  String city;

  ModelRagister(
      {required this.email,
      required this.pass,
      required this.gender,
      required this.mobile,
      required this.city});

  Map<String, dynamic> toMap() {
    return {
      "email": email,
      "pass": pass,
      "gender": gender,
      "mobile": mobile,
      "city": city,
    };
  }

  @override
  String toString() {
    return "data2{email:$email,pass:$pass,gender:$gender,mobile:$mobile,city:$city}";
  }
}
