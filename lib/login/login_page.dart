import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../Ragister/ragister_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool emailEStatus = false;
  bool emailValidate = false;
  bool passWordStatus = false;
  bool passValidate = false;
  String errorText = "";
  String errorTextPass = "";
  bool passwordvisible = false;

  @override
  void initState() {
    super.initState();
    passwordvisible = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                    "images/bg.jpg",
                  ),
                  fit: BoxFit.fill)),
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(height: 50),
              const Center(
                  child: Text("welcome back!!!!",
                      style: TextStyle(color: Colors.black, fontSize: 30))),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                    textInputAction: TextInputAction.next,
                    autofocus: true,
                    controller: email,
                    decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.email_sharp,
                            size: 35, color: Colors.black),
                        label: const Text("Email",
                            style: TextStyle(color: Colors.black)),
                        errorText: emailEStatus ? errorText : null,
                        focusedBorder: const OutlineInputBorder(
                            borderSide:
                            BorderSide(width: 5, color: Colors.black)),
                        disabledBorder: const OutlineInputBorder(
                            borderSide:
                            BorderSide(width: 5, color: Colors.black)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: const BorderSide(
                                color: Colors.black, width: 5)))),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  textInputAction: TextInputAction.next,
                    autofocus: true,
                    obscureText: passwordvisible,
                    controller: password,
                    decoration: InputDecoration(
                        errorText: passWordStatus ? errorTextPass : null,

                        suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                passwordvisible = !passwordvisible;
                              });
                            },
                            icon: Icon(
                              passwordvisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.black,
                            )),
                        prefixIcon: const Icon(Icons.lock,
                            color: Colors.black, size: 35),
                        label: const Text("Password",
                            style: TextStyle(color: Colors.black)),
                        focusedBorder: const OutlineInputBorder(
                            borderSide:
                            BorderSide(width: 5, color: Colors.black)),
                        disabledBorder: const OutlineInputBorder(
                            borderSide:
                            BorderSide(width: 5, color: Colors.black)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: const BorderSide(
                                color: Colors.black, width: 5)))),
              ),
              Card(
                shadowColor: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 60,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      emailEStatus = false;
                      emailValidate = false;
                      passWordStatus = false;
                      passValidate = false;
                      if (email.text.isEmpty) {
                        emailEStatus = true;
                        errorText = "enter the name";
                      } else if (!RegExp(r'\S+@\S+\.\S+')
                          .hasMatch(email.text)) {
                        emailEStatus = true;
                        emailValidate = true;
                        errorText = "enter the vaild email";
                      } else if (password.text.isEmpty) {
                        passWordStatus = true;
                        errorTextPass = "please enter the passWord";
                      } else if (!RegExp(
                          r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                          .hasMatch(password.text)) {
                        passWordStatus = true;
                        passValidate = true;
                        errorTextPass =
                        "enter the valid password one should upercase,\n1 number,1 lowercase, 1 special char,minmum 8 digit";
                      } else {
                        Fluttertoast.showToast(
                            msg: "successfully login",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      }
                    });
                  },
                  child: Container(
                      width: 200,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20)),
                      child: const Center(
                          child: Text(
                            "Login",
                            style: TextStyle(color: Colors.white),
                          ))),
                ),
              ),
              TextButton(
                  onPressed: () {},
                  child: const Text(
                    "Forgot Password??",
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  )),
              const SizedBox(height: 400),
              RichText(
                text: TextSpan(
                  children: [
                    const TextSpan(
                        text: "Don't have an  account?",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20)),
                    TextSpan(
                        text: "sign in",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.green,
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) {
                                return const Ragister();
                              },
                            ));
                          })
                  ],
                ),
              ),
            ]),
          )),
    );
  }
}
