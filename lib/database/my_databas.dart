import 'package:login/Ragister/ragister_page.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../model/model_class_ragister.dart';

class MyDataBase {
  static Future<Database> forGettingDatabse() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'my.db');

    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute(
          'CREATE TABLE data2 (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, pass text,gender text ,mobile text,city txt)');
    });
    return database;
  }

  static Future<void> insertData(ModelRagister data2) async {
    final Database db = await forGettingDatabse();
    await db.insert("data2", data2.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    var dataa= ModelRagister(email:"", pass: "", gender: "", mobile: "", city: "");
  }

  static  Future<void>  deleteData(int id) async {
    final Database db= await forGettingDatabse();
    await db.delete("data2", where: "id =?",whereArgs: [id]);
  }
}
