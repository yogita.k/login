import 'package:flutter/material.dart';
import 'package:login/database/my_databas.dart';
import 'package:login/login/login_page.dart';
import 'package:login/model/model_class_ragister.dart';
import 'package:login/widgets/drop_down.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class Ragister extends StatefulWidget {
  static Database? database;

  const Ragister({Key? key}) : super(key: key);

  @override
  State<Ragister> createState() => _RagisterState();
}

class _RagisterState extends State<Ragister> {
  String gender = "male";
  static TextEditingController email = TextEditingController();
  static TextEditingController pass = TextEditingController();
  static TextEditingController mobile = TextEditingController();
  static String emailData = email.text;
  static String passData = pass.text;
  static String mobileData = mobile.text;
  String errorText = "";
  bool emailStatus = false;
  bool emailValidate = false;
  bool passStatus = false;
  bool passValidate = false;
  bool mobileStatus = false;
  bool mobileValidate = false;
  bool passwordVisible = false;
  bool isChecked = false;

  @override
  void initState() {
    super.initState();
    showData();
    gender = "female";
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                    "images/bg.jpg",
                  ),
                  fit: BoxFit.fill)),
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(height: 50),
              const Center(
                  child: Text("welcome !!!!",
                      style: TextStyle(color: Colors.black, fontSize: 30))),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                    textInputAction: TextInputAction.next,
                    autofocus: true,
                    controller: email,
                    decoration: InputDecoration(
                        errorText: emailStatus ? errorText : null,
                        prefixIcon: const Icon(Icons.email_sharp,
                            size: 35, color: Colors.black),
                        label: const Text("Email",
                            style: TextStyle(color: Colors.black)),
                        focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 5, color: Colors.black)),
                        disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 5, color: Colors.black)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: const BorderSide(
                                color: Colors.black, width: 5)))),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                    autofocus: true,
                    obscureText: passwordVisible,
                    keyboardType: TextInputType.visiblePassword,
                    textInputAction: TextInputAction.next,
                    controller: pass,
                    decoration: InputDecoration(
                        alignLabelWithHint: false,
                        filled: true,
                        errorText: passStatus ? errorText : null,
                        suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            },
                            icon: Icon(
                                passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.black)),
                        prefixIcon: const Icon(Icons.lock,
                            color: Colors.black, size: 35),
                        label: const Text("Password",
                            style: TextStyle(color: Colors.black)),
                        focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 5, color: Colors.black)),
                        disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 5, color: Colors.black)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: const BorderSide(
                                color: Colors.black, width: 5)))),
              ),
              Row(children: [
                Expanded(
                  child: RadioListTile(
                    activeColor: Colors.black,
                    title: Text("male"),
                    value: "male",
                    groupValue: gender,
                    onChanged: (value) {
                      setState(() {
                        gender = value!;
                      });
                    },
                  ),
                ),
                Expanded(
                  child: RadioListTile(
                    activeColor: Colors.black,
                    title: Text("female"),
                    value: "female",
                    groupValue: gender,
                    onChanged: (value) {
                      setState(() {
                        gender = value!;
                      });
                    },
                  ),
                )
              ]),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                    textInputAction: TextInputAction.next,
                    autofocus: true,
                    controller: mobile,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        errorText: mobileStatus ? errorText : null,
                        prefixIcon: const Icon(Icons.numbers,
                            color: Colors.black, size: 35),
                        label: const Text("mobile number",
                            style: TextStyle(color: Colors.black)),
                        focusedBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 5, color: Colors.black)),
                        disabledBorder: const OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 5, color: Colors.black)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: const BorderSide(
                                color: Colors.black, width: 5)))),
              ),
              DropDown(),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(children: [
                  Checkbox(
                    activeColor: Colors.black,
                    fillColor: const MaterialStatePropertyAll(
                      Colors.black,
                    ),
                    value: isChecked,
                    onChanged: (bool? value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                  ),
                  const Text(
                      "I agreee to the terms & conditions And \n privancy Policy",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                ]),
              ),
              Card(
                shadowColor: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 60,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      emailStatus = false;
                      emailValidate = false;
                      passStatus = false;
                      passValidate = false;
                      mobileStatus = false;
                      mobileValidate = false;
                      if (email.text.isEmpty) {
                        emailStatus = true;
                        errorText = "enter the email";
                      } else if (!RegExp(r'\S+@\S+\.\S+')
                          .hasMatch(email.text)) {
                        emailStatus = true;
                        emailValidate = true;
                        errorText = "enter the vaild email";
                      } else if (pass.text.isEmpty) {
                        passStatus = true;
                        errorText = "please enter the password";
                      } else if (!RegExp(
                              r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                          .hasMatch(pass.text)) {
                        passStatus = true;
                        passValidate = true;
                        errorText =
                            "enter the valid password one should upercase,\n1 number,1 lowercase, 1 special char,minmum 8 digit";
                      } else if (mobile.text.isEmpty) {
                        mobileStatus = true;
                        errorText = "please enter mobile number";
                      } else if (!RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)')
                          .hasMatch(mobile.text)) {
                        mobileStatus = true;
                        mobileValidate = true;
                        errorText = "please enter corect number";
                      } else if (isChecked == false) {
                      } else {
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) {
                            return LoginPage();
                          },
                        ));
                      }
                      showInsertedData();
                    });
                  },
                  child: Container(
                      width: 200,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20)),
                      child: const Center(
                          child: Text(
                        "submit",
                        style: TextStyle(color: Colors.white),
                      ))),
                ),
              ),
            ]),
          )),
    );
  }

  Future<void> showData() async {
    MyDataBase.forGettingDatabse().then(((value) {
      value.execute(
          'CREATE TABLE data2 (id INTEGER PRIMARY KEY AUTOINCREMENT , email TEXT, pass text,gender text ,mobile text,city txt)');
    }));
  }

  showInsertedData() {
    MyDataBase.insertData(ModelRagister(
        email: email.text,
        pass: pass.text,
        gender: gender,
        mobile: mobile.text,
        city: DropDown.selectedValue!));
  }
}
